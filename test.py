import init
import numpy as np

# on fait une fonction qui renvoie pour un ensemble de donnees donne, une liste associant a chaque label son nombre d'occurences
def nbExPerClass (data):

    res = [0 for i in range(10)]
    
    for i in range(0,10000):
        res[data['labels'][i]] += 1

    return res

# on affiche le nombre moyen d'exemple pour chaque classe
mean = []

for data in [init.data1, init.data2, init.data3, init.data3, init.data4, init.data5]:
    res = nbExPerClass(data)
    mean.append(res)

mean = np.mean(mean, axis=0)

# on range le dictionnaire

dict = {init.nameOfLab(i) : mean[i] for i in range(0,10)}
print(dict)
