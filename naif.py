from __future__ import division

import init
import dist
from kmeanClass import kmeans
from perceptron import perceptronT

from pprint import pprint

import numpy as np

# La fonction leaderClass prend en parametre un tableau 2D, avec les images, et leurs labels.
# Il renvoie une liste composee des leaders de chaque classe, calcules en faisant la moyenne des images de la classe, composante par composante
def leaderClass (imgSet):

    res = np.zeros((10,1024*3))

    tri = [[] for i in range(0,10)]
    for i in range(0,len(imgSet['data'])):
        lab = imgSet['labels'][i]
        img = imgSet['data'][i]
        tri[lab].append(img)

    for i in range(0,10):
        res[i] = np.mean(tri[i], axis = 0)    # on fait la moyenne composante par composante de tous les vecteurs de la liste

    return res
'''
data = {}
data['data'] = init.data1['data'][:100]
data['labels'] = init.data1['labels'][:100]
init.printImg(leaderClass(data)[0])
init.printImg(leaderClass(data)[3])
'''

# la fonction distMinC classe l'image passee en param en fonction de la liste des leaders passee en params
def distMinC (img, leaders):

    labelMin = 0
    distMin = 2**53

    for i in range(len(leaders)):
        # Avec distance RGB sur tout corpus, on a 27.69% de succes
        # Avec distance euc sur tout corpus, on a 27.74% de succes
        dist_ = dist.distEuc(img,leaders[i])
        if dist_ < distMin:
            distMin = dist_
            labelMin = i

    return labelMin

def distMinTest (corpusL, corpusT):

    leaders = leaderClass(corpusL)

    n = len(corpusT['data'])
    nbErr = 0

    for i in range(0,n):

        lab = corpusT['labels'][i]
        img = corpusT['data'][i]

        if distMinC(img, leaders) != lab:
            nbErr += 1

    return 1-nbErr/n

# On fait une fonction qui prend en parametres un corpus d'apprentissage et un corpus de test, et renvoie la matrice de confusion
def matConf (corpusL, corpusT):

    leaders = leaderClass(corpusL)

    mat = np.zeros((10,10))

    for i in range(0,len(corpusT['data'])):

        img = corpusT['data'][i]
        lab = corpusT['labels'][i]

        labCalc = distMinC(img, leaders)

        if labCalc != lab:
            mat[labCalc][lab] += 1

    return mat

# On concatene les donnees et les labels de tous les fichiers de donnee pour tester sur tout le corpus
corpusL = {'data' : np.concatenate((init.data1['data'],init.data2['data'],init.data3['data'],init.data4['data'],init.data5['data'])),
           'labels' : np.concatenate((init.data1['labels'],init.data2['labels'],init.data3['labels'],init.data4['labels'],init.data5['labels']))
          }
corpusT = init.test

'''
print("Taux de succes : "+str(distMinTest(corpusL, corpusT)*100))
'''
'''
pprint(matConf(corpusL, corpusT))
'''


'''
   Truc qui sert a rien (mais c'est joli)

# Fonction qui fait le pre-traitement pour kmeans : img 32*32
def convertForKmeans (img):
    n = int(len(img)/3)
    return [[img[i], img[i+n], img[i+2*n]] for i in range(n)]

# Fonction qui fait le post-traitement pour kmeans : img 32*32
def convertFromKmeans (data):
    n = len(data)
    return [data[i][0]/255 for i in range(n)] + [data[i][1]/255 for i in range(n)] + [data[i][2]/255 for i in range(n)]

# Segmentation de l'img : prend img, renvoie img avc pix remplaces par leaders clusters
def segImg (img, k, t, maxiter):

    data = convertForKmeans(img)

    clusters, leaders = kmeans(data, k, t, maxiter)

    dataSeg = [0 for i in range(len(data))]

    for i in range(len(data)):
        pix = data[i]
        for j in range(len(clusters)):
            if pix in clusters[j]:
                dataSeg[i] = leaders[j]
                break

    imgSeg = convertFromKmeans(dataSeg)

    return imgSeg

img = init.data1['data'][9]
init.printImg(img)
init.printImg(segImg(img,16,0.001,50))
'''
# on fait comme un distMin, mais avec notre liste [(leader, lbl)] rendu par kmeans
def kmeansC (img, leadWlbl):

    labelMin = 0
    distMin = 2**53

    for i in range(len(leadWlbl)):
        # Avec distance RGB sur tout corpus, on a 27.69% de succes
        # Avec distance euc sur tout corpus, on a 27.74% de succes
        dist_ = dist.distEuc(img,leadWlbl[i][0])
        if dist_ < distMin:
            distMin = dist_
            labelMin = leadWlbl[i][1]

    return labelMin

def kmeansT (corpusL, corpusT, k, t, maxiter):

    clus, leadWlbl = kmeans(corpusL, k, t, maxiter)

    nbErr = 0

    for i in range(len(corpusT['data'])):
        img = corpusT['data'][i]
        lbl = corpusT['labels'][i]

        if kmeansC(img, leadWlbl) != lbl:
            nbErr += 1

    return nbErr / len(corpusT['data'])

'''
print("Taux de succes : "+str((1-kmeansT(corpusL, corpusT, 2, 0.01, 50))*100)+"%")
'''

# 26.95% sur data1
# 23.16% sur tout le corpus d'apprentissage
print("Taux de succes : "+str((1-perceptronT(corpusL, corpusT, 50, 10))*100)+"%")
