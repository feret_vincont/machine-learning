import random
import math
import numpy as np
import matplotlib.pyplot as plt
import operator

import dist
import init

from pprint import pprint


# We create a function choose_initial which is given a list of images, and a number k, and returns a list containing different points among data, k for each label.

def choose_initial(imgs, lbls, k):

    imgsCopy = list(imgs)
    lblsCopy = list(lbls)
    
    count = [0 for i in range(10)]
    res = []

    while min(count) != k and imgsCopy != []:

        ind = random.choice(range(len(imgsCopy)))
        chosenImg = imgsCopy[ind]
        chosenLabl = lblsCopy[ind]

        if count[chosenLabl] < k:
            count[chosenLabl] += 1
            res.append(chosenImg)

        del imgsCopy[ind]
        del lblsCopy[ind]

        ind += 1

    return res


# the kmeans function is given :
#     - data: the list of points
#     - k: nb of partitions to find
#     - t: "critere d'arret"
#     - maxiter: max nb of iterations for the algorithm
# and returns a list with lists of the points in each cluster, and a list with the leader of each partition

def kmeans (data, k, t, maxiter):

    imgs = data['data']
    labels = data['labels']
    
    leader = choose_initial(imgs, labels, k)
    
    it = 0
    diff = True
    err = t+1

    while it < maxiter and diff and err > t:

        diff = False

        num = [[] for i in range(10*k)]
        
        # repartition des points dans les clusters
        for i in range(len(imgs)):
            img = imgs[i]
            lbl = labels[i]
            distT = []
            for j in range(len(leader)):
                distT.append((dist.distEuc(img,leader[j]),j))
            num[min(distT)[1]].append((img,lbl))

        # calcul des nouveaux leaders, et verif qu'on a tjs au moins un de different
        for i in range(k):
            cluster = num[i]

            if cluster != []:
                lead = np.mean(cluster[0], axis=0)
                if lead[0] != leader[i][0] or lead[1] != leader[i][1]:
                    diff = True
                    leader[i] = lead
            else:
                leader[i] = random.choice(data_)

        # calcul de l'erreur
        new_err = 0
        
        for i in range(k):
            lead = leader[i]
            for x in num[i]:
                new_err += dist.distEuc(x[0], lead)

        err = math.fabs(new_err - err)
            
        it += 1

    leadWlbl = []      # [(leader, lbl)*]
    for i in range(len(leader)):
        c = num[i]
        l = leader[i]
        leadWlbl.append((l, init.election(c)))
        
    return num, leadWlbl
