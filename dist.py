import init
from scipy.spatial import distance


def distEuc (vec1, vec2):
    return distance.euclidean(vec1, vec2)


# On cree une fonction distRGB, qui prend en parametres deux images.
# Elle les decompose pour recuperer les vecteurs des composantes rouges, vertes, et bleues.
# Elle renvoie ensuite la moyenne entre 
def distRGB (img1, img2):
    compR1 = img1[:1024]
    compR2 = img2[:1024]
    compG1 = img1[1024:2048]
    compG2 = img2[1024:2048]
    compB1 = img1[2048:]
    compB2 = img2[2048:]
    
    return (distEuc(compR1,compR2) + distEuc(compG1,compG2) + distEuc(compB1,compB2))/3

'''
print distRGB(init.data1['data'][9],init.data1['data'][17])
'''

