from __future__ import division

import init
import matplotlib.pyplot as plt
import numpy as np

def patchImg (im):

    img = np.asarray([[im[i], im[1024+i], im[2048+i]] for i in range(0,1024)]).reshape(32,32,3)

    patches = [[] for i in range(4)]

    patches[0] = img[00:16, 00:16]
    patches[1] = img[00:16, 16:32]
    patches[2] = img[16:32, 00:16]
    patches[3] = img[16:32, 16:32]

    res = []
    
    for patch in patches:
        reds = [patch[i][j][0] for i in range(16) for j in range(16)]
        greens = [patch[i][j][1] for i in range(16) for j in range(16)]
        blues = [patch[i][j][2] for i in range(16) for j in range(16)]
        res.append(reds+greens+blues)
    
    return res

'''
data = init.data1
im = data['data'][9]
img = np.asarray([[im[i], im[1024+i], im[2048+i]] for i in range(0,1024)]).reshape(32,32,3)
plt.figure(1)
plt.imshow(img)

pat = patchImg(im)
plt.figure(2)
for k in range(4):
    im = pat[k]
    img = np.asarray([[im[i], im[256+i], im[512+i]] for i in range(0,256)]).reshape(16,16,3)
    plt.subplot(2,2,k+1)
    plt.imshow(img)
plt.show()
'''

def patchAll (l_imgs):

    temp = []
    
    for im in l_imgs:
        temp.append(patchImg(im))

    res = []
        
    for imPatch in temp:
        for patch in imPatch:
            res.append(patch)

    return res

'''
data = init.data1
imgs = data['data'][:500]
res = patchAll(imgs)

for k in range(36,40):
    im = res[k]
    img = np.asarray([[im[i], im[256+i], im[512+i]] for i in range(0,256)]).reshape(16,16,3)
    print(k-35)
    plt.subplot(2,2,k-35)
    plt.imshow(img)
plt.show()
'''
