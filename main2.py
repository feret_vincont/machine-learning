from __future__ import division

import init
import dist

from decoupe import patchAll
from decoupe import patchImg
import perceptron
from kmean import kmeans

# on fait une fonction qui prend en parametres une image, et la liste des leaders du dictionnaire de patchs, et renvoie le vecteur caracteristique de l'image
def vecCaracImg (img, leads):

    patches = patchImg(img)
    res = []
    
    for p in patches :
        distT = []
        for j in range(len(leads)):
            distT.append((dist.distEuc(p,leads[j]),j))
        i = min(distT)[1]
        res.append([1 if j == i else 0 for j in range(len(leads))])
        
    res2 = []
    for p in res:
        for j in p:
            res2.append(j)

    return res2

def vecCaracAll (data, leads):

    vects = [vecCaracImg (img, leads) for img in data['data']]
    lbls = data['labels']
    res = {'labels' : lbls, 'data' : vects}
    
    return res

corpusL = {'data' : init.data1['data'][:1000], 'labels' : init.data1['labels'][:1000]}
corpusT = {'data' : init.test['data'][:200], 'labels' : init.test['labels'][:200]}

pat = patchAll(corpusL['data'])

clus, leads = kmeans(pat, 10, 10**(-12), 500)

vecL = vecCaracAll(corpusL, leads)
vecT = vecCaracAll(corpusT, leads)

print("Taux de succes : "+str((1-perceptron.perceptronT(vecL, vecT, 50, 10))*100)+"%")
