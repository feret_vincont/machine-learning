import random
import math
import numpy as np
import matplotlib.pyplot as plt
import operator

import dist
import init

from pprint import pprint


# We create a function suppr_doublons which is given a list and returns a set of the elements

def suppr_doublons (liste):
    
    res = []

    for elt in liste:
        if elt not in res:
            res.append(elt)

    return res

# We create a function choose_initial which is given a list of coordinates and a number k, and returns a list containing k different points among data.

def choose_initial(data, k):

    set_ = range(len(data))

    res = []
    
    for i in range(k):

        indChosen = random.choice(set_)
        chosen = data[indChosen]
        res.append(chosen)
        set_.remove(indChosen)

    return res


# the kmeans function is given :
#     - data: the list of points
#     - k: nb of partitions to find
#     - t: "critere d'arret"
#     - maxiter: max nb of iterations for the algorithm
# and returns a list with lists of the points in each cluster, and a list with the leader of each partition

def kmeans (data, k, t, maxiter):

    leader = choose_initial(data, k)
    
    it = 0
    diff = True
    err = t+1

    while it < maxiter and diff and err > t:

        print("Iteration : "+str(it))
        
        diff = False

        num = [[] for i in range(10*k)]
        
        # repartition des points dans les clusters
        for x in data:
            distT = []
            for j in range(len(leader)):
                distT.append((dist.distEuc(x,leader[j]),j))
            num[min(distT)[1]].append(x)

        # calcul des nouveaux leaders, et verif qu'on a tjs au moins un de different
        for i in range(k):
            cluster = num[i]
            if cluster != []:
                lead = np.mean(cluster, axis=0)
                if any([leader[i][j] != lead[j] for j in range(len(lead))]):
                    diff = True
                    leader[i] = lead
            else:
                leader[i] = random.choice(data_)

        # calcul de l'erreur
        new_err = 0
        
        for i in range(k):
            lead = leader[i]
            for x in num[i]:
                new_err += dist.distEuc(x, lead)

        err = math.fabs(new_err - err)
            
        it += 1
        
    return num, leader
