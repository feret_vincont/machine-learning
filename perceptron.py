from __future__ import division

import numpy as np

# classify is given :
#      - obs : the x we want to label
#      - params : the list of w we want to compare x to
# and returns the num of the w for which x.w is max
def classify (obs, listParam):

    print("obs")
    print(obs)
    print("listParam")
    print(listParam[0])
    dotMax = np.dot(obs, listParam[0])
    indMax = 0

    for i in range(1,len(listParam)):
        d = np.dot(obs, listParam[i])
        if d > dotMax:
            dotMax = d
            indMax = i

    return indMax

# learn is given:
#      - corpus : learning corpus
#      - nbIt : number of iterations
#      - k : number of classes
# and return the list of the w parameters associated with each classes
def learn (corpus, nbIt, k):

    sizeImg = len(corpus['data'][0])

    listW = [[0 for i in range(sizeImg)] for i in range(k)]

    for i in range(nbIt):

        for j in range(len(corpus['data'])):

            img = np.asarray(corpus['data'][j])
            lbl = corpus['labels'][j]

            chosenLbl = classify(img, listW)

            if lbl != chosenLbl:
                print(type(listW[chosenLbl]))
                print(type(img))
                listW[chosenLbl] -= img
                listW[lbl] += img

    return listW

def perceptronT (corpusL, corpusT, nbIt, k):

    listW = learn(corpusL, nbIt, k)
    nbErr = 0

    sizeTest = len(corpusT['data'])

    for i in range(sizeTest):

        img = corpusT['data'][i]
        lbl = corpusT['labels'][i]

        if classify(img, listW) != lbl:
            nbErr += 1

    return nbErr / sizeTest
