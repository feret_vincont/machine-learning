from __future__ import division

import numpy as np
import matplotlib.pyplot as plt
import cPickle
import operator

from pprint import pprint

# definition of the unpickle function given on the cifar-10 website
def unpickle(fileAd):
    fo = open(fileAd, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

# Global variables
data1 = unpickle("./cifar-10-batches-py/data_batch_1")
data2 = unpickle("./cifar-10-batches-py/data_batch_2")
data3 = unpickle("./cifar-10-batches-py/data_batch_3")
data4 = unpickle("./cifar-10-batches-py/data_batch_4")
data5 = unpickle("./cifar-10-batches-py/data_batch_5")
test = unpickle("./cifar-10-batches-py/test_batch")
meta = unpickle("./cifar-10-batches-py/batches.meta")


# definition of a function which is given a label as an int and returns the name of the label
def nameOfLab(i):
    return meta['label_names'][i]

# definition of a function which is given an image as an array of dimension (3072) and prints that image in figure(1)
def printImg (im):

    img = np.asarray([[im[i], im[1024+i], im[2048+i]] for i in range(0,1024)]).reshape(32,32,3)
    plt.figure(1)
    plt.imshow(img)
    plt.show()

# definition of a function which is given a list of images associated with labels, and returns the labels which appear the most
def election (clus):

    d = {}
    for elt in clus:
        lbl = elt[1]
        if lbl not in d:
            d[lbl] = 1
        else:
            d[lbl] += 1

    return sorted(d.items(), key=operator.itemgetter(1), reverse = True)[0][0]
